---
title: "When to file"
linkTitle: "When to File"
weight: 1
type: docs
description: >
  When Can I File A Tenants Assertion Form With The Court?
---

## <div class="-bg-primary p-3 display-4">When can I file a Tenants Assertion?</div>

As a tenant, you can file a tenant’s assertion if your landlord is violating a provision of your lease by not providing something he or she said they would provide, like adequate parking if you are disabled or trash collection. You can also file if you don’t have hot or cold running water, light, electricity, or working plumbing and sewage, if there are rodents, mold, or lead paint, or if there is any other serious threat to your health or safety in your house or apartment such as faulty locks or doors, regardless of what your lease says.

###  <div class="-bg-200 p-3 display-6">What if I caused the problem?</div>

If you, your family, or your guest caused the problem, then you cannot file a tenant’s assertion. For example, you cannot file a tenant’s assertion if it is your responsibility to pay the water bill and your water got shut off. You also cannot file a tenant’s assertion if your child clogged the bathtub. This must be a problem that either occurred on its own, was caused other than a houshold member or household guest or visitor, or that was present when you moved in. 

## <div class="-bg-primary p-3 display-4">Ok, my issue happened on its own, what do I do now?</div>

### <div class="-bg-200 p-3 display-6">1.	Make sure you are current on your rent.</div> 

Keep paying your rent when it is due. The law in Virginia does not allow you to withhold rent from your landlord for any reason. You can pay your rent to the judge, but we will get to that in a minute.

### <div class="-bg-200 p-3 display-6">2.	Take pictures of all of the problems that you are having.</div>

These pictures will come in handy if your landlord disputes the charges. They will also help establish the timeline of events, and help you get your security deposit back if you end up moving out. 

### <div class="-bg-200 p-3 display-6">3.	Write a letter to your landlord.</div>

In order to get your landlord to make repairs to your house or apartment, you must first tell your landlord in writing that there are problems and that you will take him or her to court if they are not fixed. You should write this letter even if you have already communicated with your landlord about the problems. This notification gives your landlord the opportunity to make the repairs and avoid going to court. 

The letter should describe the problems (lack of heat, mold, or rodents), tell your landlord to fix the problems by a specific date (usually two weeks, but it can be less if you are in danger), and let your landlord know that you are prepared to file a tenant’s assertion if he or she does not fix the problems by that date. We have provided an example [hyperlink] for you to follow here.

### <div class="-bg-200 p-3 display-6">4.	Mail the letter to your landlord and keep a copy for yourself.</div>

While this may seem simple, you need to be sure that you are sending the letter to your actual landlord, not the property manager or the business that runs the apartment complex. If you are having trouble finding your landlord’s address, try (looking up your landlord’s name)[link to resources in A2J interview]. If your house or apartment is owned by a company, look up the company name on the (Virginia’s state corporation commission website)[link] and send the letter to the “registered agent”.

You also need to mail this letter from a post office and request that a return receipt be sent to you when the letter is delivered. Keep the receipt and your copy of the letter with the documents you will take to court. The judge may ask for this receipt if your landlord says he never got a letter. 

### <div class="-bg-200 p-3 display-6">5. You Must Give A Reasonable Time for Them to Respond.</div> 

Give the Landlord a reasonable amount of time to remedy the breach or dangerous condition after they receive notice of such. Generally, a Landlord is entitled to at least 30 days to remedy the breach or condition, unless it’s an essential service, like water or electricity, in which case, the tenant should resolve the issue as soon as possible.

## Next Steps 

### [File a Tenants Assertion Form With The Court](https://ura2j.gitlab.io/netwit/tenant/docs/how-to-file/) 

## More Information 

### [What Are My Rights As a Tenant?](/tenant/rights/)

