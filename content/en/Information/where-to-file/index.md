---
title: "Where to File"
linkTitle: "Where to File"
weight: 3
description: >
  Information you need to know to file your Tenants Assertion Form With The Court
---

## Steps 

1. [Notify______](#notify_______)

2. [Fill out_____________](#____________)

3. [File at your local District Court](#file-at-your-local-district-court)

4. [Set A Court Date](#set-a-court-date)


### 1. Notify_______

Add content

### 2. Give them time to fix the repair

Add content

### 3. File a Tenant Assertion and Complaint Form

*  [Click Here for a Copy of the form](Add form) 

Summary: 


#### Instructions 
*  [Click here for instructions on how to fill out this form](/page/ta_instruction) 
   For more information on this form click here. 

*  For tips on filling on your legal issue [click here](add youtube link)

*  It will cost about $_____to file and serve the papers. You may ask the clerk for ["waiver of fee"](http://www.courts.state.va.us/forms/district/dc366a.pdf) if you can’t afford to pay.
*   When filing a Tenant’s Assertion, you must be completely current on your rent. Instead of sending your rent check to the landlord, you can send it to the court until repairs are complete. (Internal Note this languge needs to be made clearer)


### 4. File at your local District Court
*  You must file the form at the local district where you live. For a map of a Map of Virginia's Judicial Circuits and Districts [Click Here](http://www.courts.state.va.us/courts/maps/home.html). For case status and case infomration [click here](https://eapps.courts.state.va.us/gdcourts/captchaVerification.do?landing=landing)


#### 5. Set A Court Date
*  The court will set  hearing date and has the landlord served with a summons to appear in court.
*  For information on what to expect when dealing with the court [click here](page/court_process)
